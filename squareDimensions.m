function [imageSquare] = squareDimensions(image, val)
% padds array with a given value so that dimensions will be equal
% Authors: Piotr Stępień
% Contact: piotr.stepien.dokt@pw.edu.pl
% Affiliation: Warsaw University of Technology, Institute of Micromechanics and Photonics

[height, width] = size(image);
if (height == width)
    imageSquare = image;
elseif (height > width)
    diffSize = height - width;
    halfDiffSize = floor(diffSize / 2);
    imageSquare = padarray(image, [0, halfDiffSize], val);
    if (mod(diffSize, 2) ~= 0)
        imageSquare = padarray(imageSquare, [0, 1], val, 'post');
    end
elseif (width > height)
    diffSize = width - height;
    halfDiffSize = floor(diffSize / 2);
    imageSquare = padarray(image, [halfDiffSize, 0], val);
    if (mod(diffSize, 2) ~= 0)
        imageSquare = padarray(imageSquare, [1, 0], val, 'post');
    end
end

end
