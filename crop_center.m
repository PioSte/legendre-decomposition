function cropped_image = crop_center(image, cropped_size, pre_or_post)

    curr_size = size(image);
    diff_size = curr_size - cropped_size;
    if diff_size(1) < 0 || diff_size(2) < 0
        error('One of dimensions is too small')
    end
    
    diff_size_half = floor(diff_size / 2);
    shift = mod(diff_size, 2);
    
    if strcmp(pre_or_post, 'pre')
        cropped_image = image(diff_size_half(1)+shift(1)+1:end-diff_size_half(1), diff_size_half(2)+shift(2)+1:end-diff_size_half(2));
    elseif strcmp(pre_or_post, 'post')
        cropped_image = image(diff_size_half(1)+1:end-diff_size_half(1)-shift(1), diff_size_half(2)+1:end-diff_size_half(2)-shift(2));
    else
        error('pre_or_post – wrong value')
    end

end