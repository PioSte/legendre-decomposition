function [recData, coeffs] = legendrePolyDecomp(ph, degree)
% Function reconstructs the image (ph) using the Legendre polynomials, up to given degree. 
% For low degree (2-3) the outcome is that only slowly varying information is reconstructed. 
% This information is often a good estimate of the optical system aberrations.
% INPUT:
    % ph     - phase image to be reconstructed in the Legendre polynomials space
    % degree - maximum degree of the Legendre polynomials to be used for the reconstruction
% OUTPUT:
    % recData - reconstructed image
    % coeffs  - Legendre polynomials coefficients
% EXAMPLE:
    % ph = holoProcessing(obj, 0, 0, 'miguel', 0.1, 1, 0.7, 0.6328, 3.45, 50, 1, 1, 0); % phase retrieval
    % [recData, coeffs] = legendrePolyDecomp(ph, degree); % aberration estimation
    % ph_comp = ph - recData; % aberration compensation
% Authors: Piotr Stępień
% Contact: piotr.stepien.dokt@pw.edu.pl
% Affiliation: Warsaw University of Technology, Institute of Micromechanics and Photonics
% 	pkg load miscellaneous
%     ph(isnan(ph)) = inf;
%     mask_inf = isinf(ph);
    [origHeight, origWidth] = size(ph);
    ph = squareDimensions(ph, NaN);
    squareSize = size(ph);
	mask = isnan(ph);
	surfSize = size(ph, 1);
	pxSize = surfSize^2 - nnz(mask);
	x = linspace(-1, 1, surfSize);
	modesSize = (1 + degree) * degree / 2;
% 	modes = zeros(surfSize, degree);
	for i = 1 : degree
		n = i - 1;
		Px(i, :) = legendreP(n, x);
		Py(:, i) = flipud(legendreP(n, x)');
% 		PxDvt(i, :) = polyval(polynomialDerivative(legendrepoly(n)), x);
% 		PyDvt(:, i) = flipud(PxDvt(i, :)');
	end
	Py = permute(repmat(Py, 1, 1, surfSize), [1, 3, 2]);
	Px = permute(repmat(Px, 1, 1, surfSize), [3, 2, 1]);
	
% 	PyDvt = permute(repmat(PyDvt, 1, 1, surfSize), [1, 3, 2]);
% 	PxDvt = permute(repmat(PxDvt, 1, 1, surfSize), [3, 2, 1]);
	
	i = 1;
	modes = zeros(pxSize, modesSize);
	modes2 = zeros(surfSize, surfSize, modesSize);
% 	modesDvtX = zeros(surfSize, surfSize, modesSize);
% 	modesDvtY = zeros(surfSize, surfSize, modesSize);
	for m = 0 : degree - 1
		for n = m : -1 : 0
			k = m - n;
			normFactor = sqrt((2 * n + 1) * (2 * k + 1)); % see SID4 Handbook v4.3.pdf (handbook for Phasics Wavefront Sensor)
			modes2(:, :, i) = normFactor * Py(:, :, k + 1) .* Px(:, :, n + 1);
			temp = reshape(modes2(:, :, i), [], 1);
            modes(:, i) = temp(mask ~= 1);
% 			modesDvtX(:, :, i) = normFactor * Py(:, :, k + 1) .* PxDvt(:, :, n + 1);
% 			modesDvtY(:, :, i) = normFactor * PyDvt(:, :, k + 1) .* Px(:, :, n + 1);
			i = i + 1;
		end
    end
    
    temp = reshape(ph, [], 1);
	coeffs = modes \ temp(mask ~= 1);
	recData = zeros(surfSize, surfSize);
% 	dvtx = zeros(surfSize, surfSize);
% 	dvty = zeros(surfSize, surfSize);

	for i = 1: size(coeffs, 1)
		recData = recData + coeffs(i) * modes2(:,:,i);
% 		dvtx = dvtx + coeffs(i) * modesDvtX(:,:,i);
% 		dvty = dvty + coeffs(i) * modesDvtY(:,:,i);
	end
	
% 	ratioX = 2 / (dx * surfSize); % strech function from normalized area to whole matrix, derivatives are decreasing by streching function
% 	ratioY = 2 / (dy * surfSize);
% 	dvtx = dvtx * ratioX;
% 	dvty = dvty * ratioY;
    recData = reshape(recData, squareSize);
    recData = crop_center(recData, [origHeight, origWidth], 'pre');
%     recData = recData(~mask);
%     recData = reshape(recData, [origHeight, origWidth]);
end
